INSERT INTO article (id, designation, quantity, price)
VALUES (1, 'Article 1', 10, 100.0),
       (2, 'Article 2', 20, 200.0),
       (3, 'Article 3', 30, 300.0);

INSERT INTO commande (id)
VALUES (1),
       (2);

INSERT INTO commande_article (commande_id, article_id)
VALUES (1, 1), -- La commande 1 a l'article 1
       (2, 2), -- La commande 2 a l'article 2
       (2, 3); -- La commande 2 a aussi l'article 3
