package tp.webService.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class CommandeDTO {
    private Long id;
    private List<Long> articleIds;
}

