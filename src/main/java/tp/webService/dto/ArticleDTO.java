package tp.webService.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ArticleDTO {
    private Long id;
    private String designation;
    private Integer quantity;
    private Double price;
    private List<Long> commandIds;

}

