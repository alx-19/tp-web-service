package tp.webService.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import tp.webService.dto.CommandeDTO;
import tp.webService.service.CommandeService;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/commandes")
public class CommandeController {

    private final CommandeService commandeService;

    @GetMapping
    public List<CommandeDTO> getAllCommandes() {
        return commandeService.getAllCommandes();
    }

    @GetMapping("/{id}")
    public CommandeDTO getCommande(@PathVariable Long id) {
        return commandeService.getCommande(id);
    }

    @PostMapping
    public CommandeDTO createCommande(@RequestBody CommandeDTO commandeDTO) {
        return commandeService.createCommande(commandeDTO);
    }

    @PutMapping("/{id}")
    public CommandeDTO updateCommande(@PathVariable Long id, @RequestBody CommandeDTO commandeDTO) {
        // Ensure that the provided ID matches the ID in the DTO
        commandeDTO.setId(id);
        return commandeService.updateCommande(commandeDTO);
    }

    @DeleteMapping("/{id}")
    public void deleteCommande(@PathVariable Long id) {
        commandeService.deleteCommande(id);
    }
}
