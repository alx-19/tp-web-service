package tp.webService.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tp.webService.model.Commande;

@Repository
public interface CommandeRepository extends JpaRepository<Commande, Long> {
}
