package tp.webService.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tp.webService.model.Article;

@Repository
public interface ArticleRepository extends JpaRepository<Article, Long> {
}
