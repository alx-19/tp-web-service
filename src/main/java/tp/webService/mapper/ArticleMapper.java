package tp.webService.mapper;


import org.springframework.stereotype.Component;
import tp.webService.dto.ArticleDTO;
import tp.webService.model.Article;
import tp.webService.model.Commande;

import java.util.stream.Collectors;

@Component
public class ArticleMapper {
    public ArticleDTO toDto(Article article) {
        ArticleDTO dto = new ArticleDTO();
        dto.setId(article.getId());
        dto.setDesignation(article.getDesignation());
        dto.setQuantity(article.getQuantity());
        dto.setPrice(article.getPrice());
        dto.setCommandIds(article.getCommandes().stream().map(Commande::getId).collect(Collectors.toList()));
        return dto;
    }

    public Article toEntity(ArticleDTO dto) {
        Article article = new Article();
        article.setId(dto.getId());
        article.setDesignation(dto.getDesignation());
        article.setQuantity(dto.getQuantity());
        article.setPrice(dto.getPrice());
        // La gestion des commandes n'est généralement pas réalisée ici, elle est généralement gérée dans le service
        return article;
    }
}
