package tp.webService.mapper;

import org.springframework.stereotype.Component;
import tp.webService.dto.CommandeDTO;
import tp.webService.model.Commande;
import tp.webService.model.Article;

import java.util.stream.Collectors;

@Component
public class CommandeMapper {
    public CommandeDTO toDto(Commande commande) {
        CommandeDTO dto = new CommandeDTO();
        dto.setId(commande.getId());
        dto.setArticleIds(commande.getArticles().stream().map(Article::getId).collect(Collectors.toList()));
        return dto;
    }

    public Commande toEntity(CommandeDTO dto) {
        Commande commande = new Commande();
        commande.setId(dto.getId());
        return commande;
    }
}

