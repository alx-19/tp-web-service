package tp.webService.service;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import tp.webService.dto.CommandeDTO;
import tp.webService.mapper.CommandeMapper;
import tp.webService.model.Article;
import tp.webService.model.Commande;
import tp.webService.repository.ArticleRepository;
import tp.webService.repository.CommandeRepository;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class CommandeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ArticleService.class);

    private final CommandeRepository commandeRepository;
    private final CommandeMapper commandeMapper;
    private final ArticleRepository articleRepository;

    public List<CommandeDTO> getAllCommandes() {
        return commandeRepository.findAll().stream()
                .map(commandeMapper::toDto)
                .collect(Collectors.toList());
    }

    public CommandeDTO getCommande(Long id) {
        Commande commande = commandeRepository.findById(id).orElse(null);
        return commande != null ? commandeMapper.toDto(commande) : null;
    }

    public CommandeDTO createCommande(CommandeDTO commandeDTO) {
        Commande commande = commandeMapper.toEntity(commandeDTO);

        List<Article> articlesToUpdate = commandeDTO.getArticleIds().stream().map(id -> {
            Article article = articleRepository.findById(id).orElse(null);
            if (article != null) {
                commande.getArticles().add(article);
                article.setQuantity(article.getQuantity() - 1);
            }
            return article;
        }).collect(Collectors.toList());

        articlesToUpdate = articleRepository.saveAll(articlesToUpdate);

        Commande savedCommande = commandeRepository.save(commande);
        return commandeMapper.toDto(savedCommande);
    }


    public CommandeDTO updateCommande(CommandeDTO commandeDTO) {
        Commande commande = commandeRepository.findById(commandeDTO.getId()).orElse(null);
        if (commande != null) {
            // Assuming that we are completely replacing the articles in the command
            commande.getArticles().clear();
            commandeDTO.getArticleIds().forEach(id -> {
                Article article = articleRepository.findById(id).orElse(null);
                if (article != null) {
                    commande.getArticles().add(article);
                    article.setQuantity(article.getQuantity() - 1);
                    articleRepository.save(article);
                }
            });
            Commande savedCommande = commandeRepository.save(commande);
            return commandeMapper.toDto(savedCommande);
        }
        return null;
    }

    public void deleteCommande(Long id) {
        commandeRepository.deleteById(id);
    }
}

