package tp.webService.service;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import tp.webService.dto.ArticleDTO;
import tp.webService.mapper.ArticleMapper;
import tp.webService.model.Article;
import tp.webService.repository.ArticleRepository;

import java.util.List;
import java.util.stream.Collectors;
@RequiredArgsConstructor
@Service
public class ArticleService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ArticleService.class);

    private final ArticleRepository articleRepository;
    private final ArticleMapper articleMapper;

    public List<ArticleDTO> getAllArticles() {
        return articleRepository.findAll().stream()
                .map(articleMapper::toDto)
                .collect(Collectors.toList());
    }

    public ArticleDTO getArticle(Long id) {
        Article article = articleRepository.findById(id).orElse(null);
        if (article == null) {
            LOGGER.error("Article with ID {} not found", id);
        }
        return article != null ? articleMapper.toDto(article) : null;
    }


    public ArticleDTO createArticle(ArticleDTO articleDTO) {
        Article article = articleMapper.toEntity(articleDTO);
        Article savedArticle = articleRepository.save(article);
        return articleMapper.toDto(savedArticle);
    }

    public ArticleDTO updateArticle(ArticleDTO articleDTO) {
        Article article = articleRepository.findById(articleDTO.getId()).orElse(null);
        if (article != null) {
            article.setDesignation(articleDTO.getDesignation());
            article.setPrice(articleDTO.getPrice());
            article.setQuantity(articleDTO.getQuantity());
            Article savedArticle = articleRepository.save(article);
            return articleMapper.toDto(savedArticle);
        }
        return null;
    }

    public void deleteArticle(Long id) {
        articleRepository.deleteById(id);
    }
}
